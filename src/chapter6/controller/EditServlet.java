package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

	    String messageId = request.getParameter("edit");
	    if (!isValidParameter(messageId, errorMessages)) {
	    	session.setAttribute("errorMessages", errorMessages);
	        response.sendRedirect("./");
	        return;
	    }
	    int id = Integer.parseInt(messageId);
	    Message message = new MessageService().select(id);

	    if (!isValidMessage(message, errorMessages)) {
	    	session.setAttribute("errorMessages", errorMessages);
	        response.sendRedirect("./");
	        return;
	    }
	    request.setAttribute("message", message);
	    request.getRequestDispatcher("edit.jsp").forward(request, response);
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		 List<String> errorMessages = new ArrayList<String>();

	     String messageId = request.getParameter("update");
	     int id = Integer.parseInt(messageId);

	     String text = request.getParameter("text");
	     Message message = new Message();
	     message.setText(text);
	     message.setId(id);
	     if (!isValid(text, errorMessages)) {
	    	 request.setAttribute("errorMessages", errorMessages);
	    	 request.setAttribute("message", message);
	    	 request.getRequestDispatcher("edit.jsp").forward(request, response);
	     }
	     new MessageService().update(message);
	     response.sendRedirect("./");
	}

	private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }
        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }

	private boolean isValidParameter(String messageId, List<String> errorMessages) {

        if (StringUtils.isBlank(messageId)) {//nullの時
            errorMessages.add("不正なパラメータが入力されました");
        } else if (!StringUtils.isNumeric(messageId)) {//数字以外の時
            errorMessages.add("不正なパラメータが入力されました");
        }
        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }

	private boolean isValidMessage(Message message, List<String> errorMessages) {

        if (message == null) {//存在しないIDの時
            errorMessages.add("不正なパラメータが入力されました");
        }
        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
